FROM node:8

RUN apt update && apt install -y mc bluetooth bluez libbluetooth-dev libudev-dev

WORKDIR /app
CMD npm install && node index.js